#!/bin/sh

#  bn-tesi-tedesco-gitpush.sh
#
# use it by terminal typing:
# bash bn-tesi-tedesco-gitpush.sh

cd /Users/davide/gitlab/SMERM/bn-tesi-tedesco

git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
